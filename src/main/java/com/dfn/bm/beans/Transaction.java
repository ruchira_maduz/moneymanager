package com.dfn.bm.beans;

public class Transaction {

    private int id;
    private int transactionType;
    private int transactionCategory;
    private double amount;
    private String date;
    private String memo;

    public Transaction() {
    }

    public Transaction(int id, int transactionType, int transactionCategory, double amount, String date, String memo) {
        this.id = id;
        this.transactionType = transactionType;
        this.transactionCategory = transactionCategory;
        this.amount = amount;
        this.date = date;
        this.memo = memo;
    }

    public Transaction(int transactionType, int transactionCategory, double amount, String date, String memo) {
        this.transactionType = transactionType;
        this.transactionCategory = transactionCategory;
        this.amount = amount;
        this.date = date;
        this.memo = memo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }

    public int getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(int transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
