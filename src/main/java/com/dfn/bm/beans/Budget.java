package com.dfn.bm.beans;

public class Budget {

    private int id;
    private double amount;
    private String date;

    public Budget() {
    }

    public Budget(double amount, String date) {
        this.amount = amount;
        this.date = date;
    }

    public Budget(int id, double amount, String date) {
        this.id = id;
        this.amount = amount;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
