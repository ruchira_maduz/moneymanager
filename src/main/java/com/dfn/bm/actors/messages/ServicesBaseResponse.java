package com.dfn.bm.actors.messages;

import com.dfn.bm.util.message.Command;
import org.json.simple.JSONObject;

public class ServicesBaseResponse extends JSONObject implements Command {

    public ServicesBaseResponse() {
    }

    public ServicesBaseResponse(JSONObject object) {
        super(object);
    }

    public int getResponseStatus() {
        return (int) get("resStatus");
    }

    public void setResponseStatus(int status) {
        put("resStatus", status);
    }

    public void setResponseData(Object data) {
        put("responData", data);
    }
}