package com.dfn.bm.actors.messages;

import akka.actor.typed.ActorRef;
import com.dfn.bm.util.message.Command;
import org.json.simple.JSONObject;

public class ServicesBaseRequest extends JSONObject implements Command {

    private JSONObject request;
    private String serviceType;
    private int functionId;
    private ActorRef<Command> replyTo;

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }

    public JSONObject getRequest() {
        return request;
    }

    public void setRequest(JSONObject request) {
        this.request = request;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public ActorRef<Command> getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(ActorRef<Command> replyTo) {
        this.replyTo = replyTo;
    }

}