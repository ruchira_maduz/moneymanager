package com.dfn.bm.actors.messages;

import akka.actor.typed.ActorRef;
import com.dfn.bm.util.message.Command;

public class WorkerTerminated implements Command {

    private ActorRef<Command> actorRef;

    public WorkerTerminated(ActorRef<Command> actorRef) {
        this.actorRef = actorRef;
    }

    public ActorRef<Command> getActorRef() {
        return actorRef;
    }
}
