package com.dfn.bm.actors.messages;

import akka.actor.typed.ActorRef;
import com.dfn.bm.util.message.Command;
import org.json.simple.JSONObject;

public class HTTPRequestCommand implements Command {

    private ActorRef<Command> replyTo;
    private JSONObject request;
    private String serviceType;
    private int functionId;

    public HTTPRequestCommand(String serviceType, int functionId, ActorRef<Command> replyTo, JSONObject request) {
        this.replyTo = replyTo;
        this.request = request;
        this.functionId = functionId;
        this.serviceType = serviceType;
    }

    public ActorRef<Command> getReplyTo() {
        return replyTo;
    }

    public JSONObject getRequest() {
        return request;
    }

    public String getServiceType() {
        return serviceType;
    }

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }
}
