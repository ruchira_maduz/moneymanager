/**
 * Utility package of Recruitment Manager Server
 */
package com.dfn.bm.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Main Utility class for performing various common util actions.
 */
public final class Utils {

    private static final Logger LOGGER = LogManager.getLogger(Utils.class);


    public static String getString(String base64) {
        String partSeparator = ",";
        if (base64.contains(partSeparator)) {
            return base64.split(partSeparator)[1];
        } else {
            return base64;
        }
    }

    public static boolean isNullOrEmptyString(String string) {
        return string == null || string.trim().isEmpty();
    }

    private Utils() {

    }

    public static String formatField(String input) {
        if (input != null) {
            return input.trim().isEmpty() ? null : input.trim();
        } else {
            return null;
        }
    }


    public static String formatFromField(String input) {
        if (input != null) {
            return input.trim().isEmpty() ? null : input.trim();
        } else {
            return null;
        }
    }


    public static String getFormatedNIC(String nic) {
        String regex = "(\\d+)";
        String number = null;
        Matcher matcher = Pattern.compile(regex).matcher(nic);
        while (matcher.find()) {
            number = matcher.group();

        }
        return number;
    }


    public static String getImageFormat(String byteString) {
        return StringUtils.substringBetween(byteString, "/", ";");
    }

    public static String getFormatFromUrl(String url) {
        String[] array = url.split("\\.");
        int size = array.length;
        return array[size - 1];
    }

    public static String arrayToString(List<Integer> list) {
        int[] array = list.stream().mapToInt(i -> i).toArray();
        return Arrays.stream(array)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(", "));
    }

    public static List<Integer> getListFromArray(String language) {
        int[] array = Arrays.stream(language.trim().split(",")).map(String::trim).mapToInt(
                Integer::parseInt).toArray();
        return Arrays.stream(array).boxed().collect(Collectors.toList());
    }

}
