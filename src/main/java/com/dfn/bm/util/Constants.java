package com.dfn.bm.util;

public class Constants {
    public static final String SERVICE_TYPE = "serviceType";
    public static final String FUNCTION_ID = "functionId";
    //Request header
    public static final String HEADER_HOST = "Host";


    public static final int INCOME_TRANSACTION = 1;
    public static final int EXPENSE_TRANSACTION = 2;


}
