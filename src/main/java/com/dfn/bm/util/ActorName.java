package com.dfn.bm.util;

/**
 * Enum for storing pre-defined actor names for easy access.
 * CAUTION! - If there are more than one actor of a certain type,
 * use the enum value with a suffix, to make the Actor name Unique.
 */
public enum ActorName {

    BUDGET_MANAGER_SUPERVISOR("BudgetManagerSupervisor"),
    HTTP_ENDPOINT_SUPERVISOR("HttpEndPointSupervisor"),
    SERVICE_REQUEST_WORKER("ServiceRequestWorker"),
    SERVICE_RESPONSE_WORKER("ServiceResponseWorker");

    /**
     * Actor's name.
     */
    private final String name;

    /**
     * @param name - Actor's name.
     */
    ActorName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
