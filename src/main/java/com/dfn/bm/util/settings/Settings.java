package com.dfn.bm.util.settings;

import com.dfn.bm.util.HTTPEndpointSettings;

/**
 * This class represents the main settings object which encapsulates all the settings defined by admin users.
 */

public class Settings {

    private static final Settings INSTANCE = new Settings();

    private DBSettings dbSettings;

    private HTTPEndpointSettings httpEndpointSettings;

    public HTTPEndpointSettings getHttpEndpointSettings() {
        return httpEndpointSettings;
    }

    public void setHttpEndpointSettings(HTTPEndpointSettings httpEndpointSettings) {
        this.httpEndpointSettings = httpEndpointSettings;
    }

    public static Settings getInstance() {
        return INSTANCE;
    }

    public DBSettings getDbSettings() {
        return dbSettings;
    }

    public void setDbSettings(DBSettings dbSettings) {
        this.dbSettings = dbSettings;
    }
}
