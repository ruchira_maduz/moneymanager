package com.dfn.bm.util.settings;

/**
 * Trade/Recovery Database class.
 */
public class DBSettings {
    /**
     * Database Type.
     */
    private String databaseType;

    /**
     * Database URL.
     */
    private String databaseURL;

    /**
     * Database dbName.
     */
    private String databaseName;

    /**
     * Database driverType.
     */
    private String driverType;

    /**
     * Database username.
     */
    private String username;

    /**
     * Database password.
     */
    private String password;

    /**
     * Database batch size.
     */
    private int batchSize;

    /**
     * Database persistence delay time in seconds.
     */
    private int persistenceDelay;

    /**
     * Database Connection Pool Size.
     */
    private int connectionPoolSize = 50;

    /**
     * Database Connection Timeout.
     */
    private int connectionTimeOut = 300;

    /**
     * @return - Database Type.
     */
    public String getDatabaseType() {
        return databaseType;
    }

    /**
     * @param databaseType - DB URL.
     */
    public void setDatabaseType(String databaseType) {
        this.databaseType = databaseType;
    }

    /**
     * @return - Database URL.
     */
    public String getDatabaseURL() {
        return databaseURL;
    }

    /**
     * @param databaseURL - DB URL.
     */
    public void setDatabaseURL(String databaseURL) {
        this.databaseURL = databaseURL;
    }

    /**
     * @return - Database Name.
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * @param databaseName - DB Name.
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @return - Driver Type.
     */
    public String getDriverType() {
        return driverType;
    }

    /**
     * @param driverType - Driver Type.
     */
    public void setDriverType(String driverType) {
        this.driverType = driverType;
    }

    /**
     * @return - Database username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username - DB Username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return - Database password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password - DB Password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return - Database batch size.
     */
    public int getBatchSize() {
        return batchSize;
    }

    /**
     * @param batchSize - DB batch size.
     */
    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    /**
     * @return - Database Persistence Delay Time.
     */
    public int getPersistenceDelay() {
        return persistenceDelay;
    }

    /**
     * @param persistenceDelay - DB Persistence Delay.
     */
    public void setPersistenceDelay(int persistenceDelay) {
        this.persistenceDelay = persistenceDelay;
    }


    public int getConnectionPoolSize() {
        return connectionPoolSize;
    }

    public void setConnectionPoolSize(int connectionPoolSize) {
        this.connectionPoolSize = connectionPoolSize;
    }

    public int getConnectionTimeOut() {
        return connectionTimeOut;
    }

    public void setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
    }
}
