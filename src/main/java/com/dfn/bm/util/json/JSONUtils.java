package com.dfn.bm.util.json;

/**
 * Utility interface for supporting different JSON Parsers.
 */
public interface JSONUtils {

    <T> String toJson(Object data, Class<T> classOfT);

    String toJson(Object data);

    <T> T fromJson(String json, Class<T> classOfT);
}
