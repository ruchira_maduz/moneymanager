package com.dfn.bm.util;

import com.dfn.bm.util.settings.Settings;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Settings reader for YML file type.
 */
public final class SettingsReader {

    private static final Logger LOGGER = LogManager.getLogger(SettingsReader.class);


    /**
     * Private constructor.
     */
    private SettingsReader() {

    }

    /**
     * @param filePath - Path to the YML settings file.
     * @return - AthenaSettings instance parsed from YML format settings file.
     * @throws SettingsNotFoundException - Settings file not found.
     */
    public static Settings loadSettings(String filePath) throws SettingsNotFoundException {
        Yaml yaml = new Yaml();
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(filePath);
        } catch (Exception e) {
            throw new SettingsNotFoundException();
        }
        return yaml.loadAs(inputStream, Settings.class);
    }

    /**
     * @param filePath - Path to the Akka config file.
     * @return - Akka Config instance parsed from Akka config file.
     * @throws SettingsNotFoundException - Settings file not found.
     */
    public static Config loadAkkaSettings(String filePath) throws SettingsNotFoundException {
        String akkaConfig;
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            akkaConfig = new String(encoded, StandardCharsets.UTF_8);

        } catch (Exception e) {
            LOGGER.error("Error found while reading settings. {} Current directory. {}, Path. {}",
                    e,
                    new File(filePath).getAbsolutePath(),
                    filePath);
            throw new SettingsNotFoundException();
        }
        Config userConfig = ConfigFactory.parseString(akkaConfig);
        Config defaultOverrides = ConfigFactory.defaultOverrides();
        Config defaultReference = ConfigFactory.defaultReference();
        Config combined = defaultOverrides.withFallback(userConfig).withFallback(defaultReference);
        return ConfigFactory.load(combined);
    }
}
