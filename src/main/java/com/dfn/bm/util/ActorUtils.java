package com.dfn.bm.util;

import com.dfn.bm.util.message.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Utility class related to handling Actor System based utility functions.
 */
public final class ActorUtils {

    private static final ThreadLocalRandom LOCAL_RANDOM_GENERATOR = ThreadLocalRandom.current();
    private static final Logger LOGGER = LogManager.getLogger(ActorUtils.class);
    private static final String SYSTEM_PATH = "/user/";
    private static akka.actor.typed.ActorSystem<Command> rootSupervisor;
    private static akka.actor.typed.ActorRef<Command> servicesSupervisor;

    private static AtomicLong atomicLong = new AtomicLong();

    private ActorUtils() {

    }

    public static String generateUniqueId() {
        long timeID = System.currentTimeMillis();
        long uniqueID = atomicLong.incrementAndGet();
        String nodeID = "A";
        return "" + nodeID + "" + timeID + "" + uniqueID;
    }


    public static akka.actor.typed.ActorRef<Command> getServicesSupervisor() {
        return servicesSupervisor;
    }

    public static void setServicesSupervisor(akka.actor.typed.ActorRef<Command> servicesSupervisor) {
        ActorUtils.servicesSupervisor = servicesSupervisor;
    }

    public static akka.actor.typed.ActorSystem<Command> getRootSupervisor() {
        return rootSupervisor;
    }

    public static void setRootSupervisor(akka.actor.typed.ActorSystem<Command> rootSupervisor) {
        ActorUtils.rootSupervisor = rootSupervisor;
    }

}
