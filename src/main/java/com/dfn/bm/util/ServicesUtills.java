package com.dfn.bm.util;

import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.Transaction;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.stream.Collectors;

public class ServicesUtills {

    public static void generateErrorResponse(JSONObject object, Object errorConstant) {
        JSONObject errorObject = new JSONObject();
        object.put("resStatus", 0);
        errorObject.put("Error", "Error: " + errorConstant);
        object.put("responData", errorObject);
    }

    public static void generateSuccussResponse(JSONObject object, Object message) {
        object.put("resStatus", 1);
        object.put("responData", message);
    }

    public static ServicesBaseResponse getResponse(boolean status, Object data) {
        ServicesBaseResponse response = new ServicesBaseResponse();
        response.put("Status", status);
        response.put("response", data);

        return response;
    }

    public static JSONObject getListObject(List<Transaction> transactionList, boolean includeIncome, boolean includeExpense) {

        JSONObject response = new JSONObject();
        double incomeSum = transactionList.stream().filter(o -> o.getTransactionType() == 1).
                collect(Collectors.summingDouble(o -> o.getAmount()));
        double expenseSum = transactionList.stream().filter(o -> o.getTransactionType() == 2).
                collect(Collectors.summingDouble(o -> o.getAmount()));

        response.put("transactionList", transactionList);
        if (includeIncome) {
            response.put("income", incomeSum);
        }
        if (includeExpense) {
            response.put("expense", expenseSum);
        }

        return response;
    }

}
