package com.dfn.bm.util.service;

import com.dfn.bm.beans.Transaction;

import java.util.List;

public interface TypeTransactionService {

    public List<Transaction> getAllTransactions(int transactionType);

    public List<Transaction> getTransactionByMonth(int month, int transactionType);
}
