package com.dfn.bm.util.service;

import com.dfn.bm.beans.Category;
import com.dfn.bm.connector.database.DBHandler;
import org.jdbi.v3.core.Handle;

import java.util.List;

public abstract class CategoryProcessor implements TransactionCategoryService {

    public void insertCategory(Category category, Handle handle) {
        DBHandler.getInstance().insertIntoCategoryTable(handle, category);
    }

    public void deleteCategory(int id, Handle handle) {
        DBHandler.getInstance().deleteFromCategoryTable(handle, id);
    }

    public void updateCategory(String name, int id, Handle handle) {
        DBHandler.getInstance().updateCategoryTable(handle, name, id);
    }

    public List<Category> getAllCategory() {
        return DBHandler.getInstance().getAllCategory();
    }

    public List<Category> getCategoriesByTransactionType(int transactionType) {
        return DBHandler.getInstance().getCategoryByTransactionCategory(transactionType);
    }
}
