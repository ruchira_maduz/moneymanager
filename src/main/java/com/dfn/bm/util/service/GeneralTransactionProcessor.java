package com.dfn.bm.util.service;

import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.Transaction;
import com.dfn.bm.connector.database.DBHandler;
import com.dfn.bm.connector.database.DBUtilStore;
import com.dfn.bm.helper.ServiceTypes;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.exception.TransactionManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.json.simple.JSONObject;

import java.util.List;

public class GeneralTransactionProcessor implements ServiceProcessor, TransactionServices {

    private static final Logger LOGGER = LogManager.getLogger(GeneralTransactionProcessor.class);
    private static GeneralTransactionProcessor INSTANCE = new GeneralTransactionProcessor();
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();

    private GeneralTransactionProcessor() {

    }

    public static GeneralTransactionProcessor getInstance() {
        return INSTANCE;
    }

    @Override
    public ServicesBaseResponse process(Object params, int functionId) {

        switch (functionId) {
            case ServiceTypes.ADD_TRANSACTION:
                return processAddTransaction(params);
            case ServiceTypes.DELETE_TRANSACTION:
                return processDeleteTransaction(params);
            case ServiceTypes.EDIT_TRANSACTION:
                return processUpdateTransaction(params);
            case ServiceTypes.GET_ALL_TRANSACTIONS:
                return processAllTransactions(params);
        }
        throw new TransactionManagerRuntimeException("Unknown Function Type Function ID: " + functionId);
    }

    private ServicesBaseResponse processAddTransaction(Object params) {
        JSONObject request = (JSONObject) params;
        int transactionType = ((Number) request.get("transactionType")).intValue();
        int transactionCategory = ((Number) request.get("transactionCategory")).intValue();
        double amount = ((Number) request.get("amount")).doubleValue();
        String date = request.get("date").toString();
        String memo = request.get("memo") != null ? request.get("memo").toString() : null;

        Transaction transaction = new Transaction(transactionType, transactionCategory, amount, date, memo);

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                insertTransaction(transaction, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse processDeleteTransaction(Object params) {
        JSONObject request = (JSONObject) params;
        int id = ((Number) request.get("id")).intValue();

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                deleteTransaction(id, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse processUpdateTransaction(Object params) {

        JSONObject request = (JSONObject) params;
        int transactionType = ((Number) request.get("transactionType")).intValue();
        int id = ((Number) request.get("id")).intValue();
        int transactionCategory = ((Number) request.get("transactionCategory")).intValue();
        double amount = ((Number) request.get("amount")).doubleValue();
        String date = request.get("date").toString();
        String memo = request.get("memo") != null ? request.get("memo").toString() : null;

        Transaction transaction = new Transaction(transactionType, transactionCategory, amount, date, memo);

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                updateTransaction(transaction, id, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse processAllTransactions(Object params) {

        try {
            List<Transaction> transactionList = getAllTransactions();
            return ServicesUtills.getResponse(true, ServicesUtills.getListObject(transactionList, true, true));

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse processAllTransactionsByMonth(Object params) {

        JSONObject request = (JSONObject) params;
        int month = ((Number) request.get("month")).intValue();

        try {
            List<Transaction> transactionList = getTransactionByMonth(month);
            return ServicesUtills.getResponse(true, ServicesUtills.getListObject(transactionList, true, true));

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }


    @Override
    public void insertTransaction(Transaction transaction, Handle handle) {
        DBHandler.getInstance().insertIntoTransactionTable(handle, transaction);
    }


    public void deleteTransaction(int id, Handle handle) {
        DBHandler.getInstance().deleteFromTransactionTable(handle, id);
    }

    public void updateTransaction(Transaction transaction, int id, Handle handle) {
        DBHandler.getInstance().updateTransactionTable(handle, transaction, id);
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return DBHandler.getInstance().getAllTransaction();
    }

    @Override
    public List<Transaction> getTransactionByMonth(int month) {
        return DBHandler.getInstance().getTransactionByMonth(month);
    }
}
