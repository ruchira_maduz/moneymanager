package com.dfn.bm.util.service;

import com.dfn.bm.beans.Transaction;
import org.jdbi.v3.core.Handle;

import java.util.List;

public interface TransactionServices {

    public void insertTransaction(Transaction transaction, Handle handle);

    public void deleteTransaction(int id, Handle handle);

    public void updateTransaction(Transaction transaction, int id, Handle handle);

    public List<Transaction> getAllTransactions();

    public List<Transaction> getTransactionByMonth(int month);


}
