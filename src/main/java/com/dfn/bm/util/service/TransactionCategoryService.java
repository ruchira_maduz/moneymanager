package com.dfn.bm.util.service;

import com.dfn.bm.beans.Budget;
import com.dfn.bm.beans.Category;
import org.jdbi.v3.core.Handle;

import java.util.List;

public interface TransactionCategoryService {

    public void insertCategory(Category category, Handle handle);

    public void deleteCategory(int id, Handle handle);

    public void updateCategory(String name,int id, Handle handle);

    public List<Category> getAllCategory();

    public List<Category> getCategoriesByTransactionType(int transactionType);

}
