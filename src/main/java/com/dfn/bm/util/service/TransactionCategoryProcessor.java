package com.dfn.bm.util.service;

import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.Category;
import com.dfn.bm.connector.database.DBUtilStore;
import com.dfn.bm.helper.ServiceTypes;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.exception.TransactionManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.json.simple.JSONObject;

import java.util.List;

public class TransactionCategoryProcessor extends CategoryProcessor implements ServiceProcessor {

    private static final Logger LOGGER = LogManager.getLogger(TransactionCategoryProcessor.class);
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();

    private static TransactionCategoryProcessor INSTANCE = new TransactionCategoryProcessor();

    private TransactionCategoryProcessor() {

    }

    public static TransactionCategoryProcessor getInstance() {
        return INSTANCE;
    }

    @Override
    public ServicesBaseResponse process(Object params, int functionId) {
        switch (functionId) {
            case ServiceTypes.ADD_CATEGORY:
                return processAddCategory(params);
            case ServiceTypes.DELETE_CATEGORY:
                return processDeleteCategory(params);
            case ServiceTypes.EDIT_CATEGORY:
                return processEditCategory(params);
            case ServiceTypes.GET_CATEGORY_BY_TRANSACTION_TYPE:
                return getCategoriesByTransactionType(params);
            case ServiceTypes.GET_ALL_TRANSACTION_CATEGORIES:
                return getAllCategories();
        }
        throw new TransactionManagerRuntimeException("Unknown Function Type Function ID: " + functionId);
    }

    private ServicesBaseResponse processAddCategory(Object params) {

        JSONObject request = (JSONObject) params;
        int transactionCategory = ((Number) request.get("transactionCategory")).intValue();
        String name = request.get("name").toString();

        Category category = new Category(transactionCategory, name);

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                insertCategory(category, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse processEditCategory(Object params) {

        JSONObject request = (JSONObject) params;
        int id = ((Number) request.get("id")).intValue();
        String name = request.get("name").toString();

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                updateCategory(name, id, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse processDeleteCategory(Object params) {

        JSONObject request = (JSONObject) params;
        int id = ((Number) request.get("id")).intValue();

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                deleteCategory(id, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse getAllCategories() {

        try {
            List<Category> categoryList = getAllCategory();
            return ServicesUtills.getResponse(true, categoryList);

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse getCategoriesByTransactionType(Object params) {

        JSONObject request = (JSONObject) params;
        int transactionCategory = ((Number) request.get("transactionCategory")).intValue();

        try {
            List<Category> categoryList = getCategoriesByTransactionType(transactionCategory);
            return ServicesUtills.getResponse(true, categoryList);

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }


}
