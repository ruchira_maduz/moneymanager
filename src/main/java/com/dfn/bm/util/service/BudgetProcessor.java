package com.dfn.bm.util.service;

import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.Budget;
import com.dfn.bm.beans.Transaction;
import com.dfn.bm.connector.database.DBHandler;
import com.dfn.bm.connector.database.DBUtilStore;
import com.dfn.bm.helper.ServiceTypes;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.exception.TransactionManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.json.simple.JSONObject;

import java.util.List;

public class BudgetProcessor implements ServiceProcessor, BudgetService {

    private static final Logger LOGGER = LogManager.getLogger(BudgetProcessor.class);
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();

    private static BudgetProcessor INSTANCE = new BudgetProcessor();

    private BudgetProcessor() {

    }

    public static BudgetProcessor getInstance() {
        return INSTANCE;
    }


    @Override
    public ServicesBaseResponse process(Object params, int functionId) {
        switch (functionId) {
            case ServiceTypes.ADD_BUDGET:
                return processAddBudget(params);
            case ServiceTypes.EDIT_BUDGET:
                return processUpdateBudget(params);
            case ServiceTypes.DELETE_BUDGET:
                return processDeleteBudget(params);
            case ServiceTypes.GET_BUDGET_BY_MONTH:
                return getBudget(params);
        }
        throw new TransactionManagerRuntimeException("Unknown Function Type Function ID: " + functionId);
    }

    private ServicesBaseResponse processAddBudget(Object params) {

        JSONObject request = (JSONObject) params;
        double amount = ((Number) request.get("amount")).doubleValue();
        String date = request.get("date").toString();
        Budget budget = new Budget(amount, date);

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                insertBudget(budget, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse processDeleteBudget(Object params) {

        JSONObject request = (JSONObject) params;
        int id = ((Number) request.get("id")).intValue();

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                deleteBudget(id, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }


    private ServicesBaseResponse processUpdateBudget(Object params) {

        JSONObject request = (JSONObject) params;
        int id = ((Number) request.get("id")).intValue();
        double amount = ((Number) request.get("amount")).doubleValue();

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                updateBudget(amount, id, handle);
                handle.commit();
                return ServicesUtills.getResponse(true, null);
            } catch (Exception e) {
                handle.rollback();
                throw new TransactionManagerRuntimeException();
            }

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse getBudget(Object params) {

        JSONObject request = (JSONObject) params;
        int month = ((Number) request.get("month")).intValue();

        try {
            Budget budget = getBudgetByMonth(month);
            return ServicesUtills.getResponse(true, budget);
        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }


    @Override
    public void insertBudget(Budget budget, Handle handle) {
        DBHandler.getInstance().insertIntoBudgetTable(handle, budget);
    }

    @Override
    public void deleteBudget(int id, Handle handle) {
        DBHandler.getInstance().deleteFromBudgetTable(handle, id);
    }

    @Override
    public void updateBudget(double amount, int id, Handle handle) {
        DBHandler.getInstance().updateBudgetTable(handle, amount, id);
    }

    @Override
    public List<Budget> getAllBudgets() {
        return DBHandler.getInstance().getAllBudget();
    }

    @Override
    public Budget getBudgetByMonth(int month) {
        return DBHandler.getInstance().getBudgetByMonth(month);
    }
}
