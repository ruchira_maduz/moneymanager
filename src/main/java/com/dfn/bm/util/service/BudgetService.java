package com.dfn.bm.util.service;

import com.dfn.bm.beans.Budget;
import com.dfn.bm.beans.Transaction;
import org.jdbi.v3.core.Handle;

import java.util.List;

public interface BudgetService {

    public void insertBudget(Budget budget, Handle handle);

    public void deleteBudget(int id, Handle handle);

    public void updateBudget(double amount, int id, Handle handle);

    public List<Budget> getAllBudgets();

    public Budget getBudgetByMonth(int month);
}
