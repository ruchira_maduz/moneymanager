package com.dfn.bm.util.service;

import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.Transaction;
import com.dfn.bm.helper.ServiceTypes;
import com.dfn.bm.util.Constants;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.exception.TransactionManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.List;

public class ExpenditureTransactionProcessor extends TransactionProcessorByType implements ServiceProcessor {


    private static final Logger LOGGER = LogManager.getLogger(ExpenditureTransactionProcessor.class);

    private static ExpenditureTransactionProcessor INSTANCE = new ExpenditureTransactionProcessor();

    private ExpenditureTransactionProcessor() {

    }

    public static ExpenditureTransactionProcessor getInstance() {
        return INSTANCE;
    }

    @Override
    public ServicesBaseResponse process(Object params, int functionId) {
        switch (functionId) {
            case ServiceTypes.GET_TRANSACTION_BY_TYPE:
                return getAllIExpenseTransaction();
            case ServiceTypes.GET_TRANSACTION_BY_MONTH_BY_TYPE:
                return getAllIExpenseTransaction(params);
        }

        throw new TransactionManagerRuntimeException("Unknown Function Type Function ID: " + functionId);
    }

    private ServicesBaseResponse getAllIExpenseTransaction() {
        try {
            List<Transaction> transactionList = getAllTransactions(Constants.EXPENSE_TRANSACTION);
            return ServicesUtills.getResponse(true, ServicesUtills.getListObject(transactionList, false, true));

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

    private ServicesBaseResponse getAllIExpenseTransaction(Object params) {

        JSONObject request = (JSONObject) params;
        int month = ((Number) request.get("month")).intValue();

        try {
            List<Transaction> transactionList = getTransactionByMonth(Constants.EXPENSE_TRANSACTION, month);
            return ServicesUtills.getResponse(true, ServicesUtills.getListObject(transactionList, false, true));

        } catch (Exception e) {
            throw new TransactionManagerRuntimeException();
        }
    }

}
