package com.dfn.bm.util.service;

import com.dfn.bm.beans.Transaction;
import com.dfn.bm.connector.database.DBHandler;

import java.util.List;

public abstract class TransactionProcessorByType implements TypeTransactionService {

    @Override
    public List<Transaction> getAllTransactions(int transactionType) {
        return DBHandler.getInstance().getTransactionsByTransactionType(transactionType);
    }

    @Override
    public List<Transaction> getTransactionByMonth(int transactionType, int month) {
        return DBHandler.getInstance().getTransactionsByTransactionTypeAndMonth(transactionType, month);
    }
}
