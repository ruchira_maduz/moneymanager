package com.dfn.bm.util.service;

import com.dfn.bm.actors.messages.ServicesBaseResponse;

public interface ServiceProcessor {
    public ServicesBaseResponse process(Object params, int functionId);
}
