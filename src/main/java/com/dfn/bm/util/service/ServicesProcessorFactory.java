package com.dfn.bm.util.service;

import com.dfn.bm.helper.ServiceTypes;
import com.dfn.bm.util.exception.TransactionManagerRuntimeException;

public class ServicesProcessorFactory {
    public static ServiceProcessor getProcessor(String serviceType) {
        switch (serviceType) {
            case ServiceTypes.GENERAL_TRANSACTION_SERVICE:
                return GeneralTransactionProcessor.getInstance();
            case ServiceTypes.INCOME_TRANSACTION_SERVICE:
                return IncomeTransactionProcessor.getInstance();
            case ServiceTypes.EXPENDITURE_TRANSACTION_SERVICE:
                return ExpenditureTransactionProcessor.getInstance();
            case ServiceTypes.TRANSACTION_CATEGORY_SERVICE:
                return TransactionCategoryProcessor.getInstance();
            case ServiceTypes.BUDGET_SERVICE:
                return BudgetProcessor.getInstance();

        }
        throw new TransactionManagerRuntimeException();
    }

}
