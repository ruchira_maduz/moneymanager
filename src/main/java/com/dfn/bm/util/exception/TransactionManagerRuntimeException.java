package com.dfn.bm.util.exception;

/**
 * Specialized Run Time Exception for Run time exceptions thrown by Recrutement Manager.
 */

public class TransactionManagerRuntimeException extends RuntimeException {

    public TransactionManagerRuntimeException() {

    }

    public TransactionManagerRuntimeException(Exception e) {
        super(e);
    }

    public TransactionManagerRuntimeException(String message) {
        super(message);
    }
}
