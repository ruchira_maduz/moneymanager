package com.dfn.bm.util;

public class SecureConnectionSettings {

    private String keyStorePath;
    private String passPhrase;
    private boolean passPhraseEncrypt;

    public String getKeyStorePath() {
        return keyStorePath;
    }

    public void setKeyStorePath(String keyStorePath) {
        this.keyStorePath = keyStorePath;
    }

    public String getPassPhrase() {
        return passPhrase;
    }

    public void setPassPhrase(String passPhrase) {
        this.passPhrase = passPhrase;
    }

    public boolean getPassPhraseEncrypt() {
        return passPhraseEncrypt;
    }

    public void setPassPhraseEncrypt(boolean passPhraseEncrypt) {
        this.passPhraseEncrypt = passPhraseEncrypt;
    }
}
