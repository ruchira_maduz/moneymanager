package com.dfn.bm.supervisors;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.DispatcherSelector;
import akka.actor.typed.SupervisorStrategy;
import akka.actor.typed.javadsl.*;
import com.dfn.bm.actors.messages.*;
import com.dfn.bm.connector.httpserver.HTTPEndpoint;
import com.dfn.bm.connector.worker.ServiceEndPointWorker;
import com.dfn.bm.util.ActorName;
import com.dfn.bm.util.message.Command;
import com.dfn.bm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
public class HTTPEndpointSupervisor extends AbstractBehavior<Command> {

    private static final Logger LOGGER = LogManager.getLogger(HTTPEndpointSupervisor.class);
    private ActorRef<Command> requestWorkersPool = null;

    public HTTPEndpointSupervisor(ActorContext<Command> context) {
        super(context);
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder().
                onMessage(StartHttpPServer.class, this::onStartHttpServer).
                onMessage(HTTPRequestCommand.class, this::onHttpRequest).build();
    }

    public static Behavior<Command> create() {
        LOGGER.info("HTTPEndpointSupervisor is started.");
        return Behaviors.setup(HTTPEndpointSupervisor::new);
    }


    protected Behavior<Command> onHttpRequest(HTTPRequestCommand servicesCommand) {
        try {
            JSONObject params = servicesCommand.getRequest();
            String serviceType = servicesCommand.getServiceType();
            int functionID = servicesCommand.getFunctionId();
            LOGGER.info("Service request received: Function ID {} Params {}", serviceType, params);

            if (requestWorkersPool == null) {
                createRequestPool();
            }

            ServicesBaseRequest request = new ServicesBaseRequest();
            request.setRequest(params);
            request.setFunctionId(functionID);
            request.setServiceType(serviceType);
            request.setReplyTo(servicesCommand.getReplyTo());
            requestWorkersPool.tell(request);
        } catch (Exception e) {
            LOGGER.error("Error in Service Supervisor Endpoint, {}", e, e);
        }
        return Behaviors.same();
    }

    private Behavior<Command> onStartHttpServer(StartHttpPServer startHttpServer) {
        if (requestWorkersPool == null) {
            createRequestPool();
        }

        return Behaviors.same();
    }


    private void createRequestPool() {
        int requestPoolSize = Settings.getInstance().getHttpEndpointSettings().getRequestWorkerPoolSize();

        PoolRouter<Command> requestPool = Routers.pool(
                requestPoolSize,
                // make sure the workers are restarted if they fail
                Behaviors.supervise(ServiceEndPointWorker.create("DEFAULT"))
                        .onFailure(SupervisorStrategy.resume()))
                .withRouteeProps(DispatcherSelector.blocking()).withRoundRobinRouting();

        requestWorkersPool = getContext().spawn(requestPool, ActorName.SERVICE_REQUEST_WORKER.toString());
        getContext().watchWith(requestWorkersPool, new WorkerTerminated(requestWorkersPool));
        LOGGER.info("Created Request Workers Pool: {}", requestWorkersPool);

    }
}
