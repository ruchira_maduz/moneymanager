package com.dfn.bm.supervisors;

import akka.actor.typed.*;
import akka.actor.typed.javadsl.*;
import com.dfn.bm.actors.messages.StartHttpPServer;
import com.dfn.bm.actors.messages.StartSupervisorCommand;
import com.dfn.bm.connector.httpserver.HTTPEndpoint;
import com.dfn.bm.util.ActorName;
import com.dfn.bm.util.ActorUtils;
import com.dfn.bm.util.message.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BudgetManagerSupervisor extends AbstractBehavior<Command> {


    private static final Logger LOGGER = LogManager.getLogger(BudgetManagerSupervisor.class);

    public BudgetManagerSupervisor(ActorContext<Command> context) {
        super(context);
    }

    public static Behavior<Command> create() {
        LOGGER.info("Budget Manager Supervisor started.");
        return Behaviors.setup(BudgetManagerSupervisor::new);
    }



    public Receive<Command> createReceive() {
        return newReceiveBuilder().
                onMessage(StartSupervisorCommand.class, this::startSupervisors).
                onSignal(PreRestart.class, signal -> onPreStart()).
                onSignal(ChildFailed.class, signal -> onChildFailed()).
                onSignal(PostStop.class, signal -> onPostStop()).build();
    }

    private Behavior<Command> startSupervisors(StartSupervisorCommand command) {

        ActorRef<Command> httpEndPointSupervisor = getContext().
                spawn(HTTPEndpointSupervisor.create(), ActorName.HTTP_ENDPOINT_SUPERVISOR.toString());

        ActorUtils.setServicesSupervisor(httpEndPointSupervisor);
        httpEndPointSupervisor.tell(new StartHttpPServer());
        LOGGER.info("HTTP EndPoint Supervisor is created {}", httpEndPointSupervisor);

        HTTPEndpoint.getSharedInstance().addServicesAPIRequestRoute();

        return Behaviors.same();
    }

    public BudgetManagerSupervisor onPostStop() {
        HTTPEndpoint.getSharedInstance().unbindThePort();
        LOGGER.info("Budget Manager Supervisor is stopped.");
        System.exit(1);
        return this;
    }

    public BudgetManagerSupervisor onPreStart() {
        LOGGER.info("Budget Manager Supervisor is PreStart.");
        return this;
    }

    public BudgetManagerSupervisor onChildFailed() {
        LOGGER.warn("Budget Manager Supervisor Child failed");
        return this;
    }
}
