package com.dfn.bm.helper;

public class ServiceTypes {

    public static final String GENERAL_TRANSACTION_SERVICE = "GENERAL";
    public static final String INCOME_TRANSACTION_SERVICE = "INCOME";
    public static final String EXPENDITURE_TRANSACTION_SERVICE = "EXPENSE";
    public static final String TRANSACTION_CATEGORY_SERVICE = "CATEGORY";
    public static final String BUDGET_SERVICE = "BUDGET";

    public static final int ADD_TRANSACTION = 1;
    public static final int EDIT_TRANSACTION = 2;
    public static final int DELETE_TRANSACTION = 3;


    public static final int GET_ALL_TRANSACTIONS = 4;
    public static final int GET_TRANSACTION_BY_MONTH = 5;
    public static final int GET_TRANSACTION_BY_MONTH_BY_TYPE = 6;
    public static final int GET_TRANSACTION_BY_TYPE = 7;

    public static final int ADD_CATEGORY = 1;
    public static final int EDIT_CATEGORY = 2;
    public static final int DELETE_CATEGORY = 3;
    public static final int GET_ALL_TRANSACTION_CATEGORIES = 4;
    public static final int GET_CATEGORY_BY_TRANSACTION_TYPE = 5;

    public static final int ADD_BUDGET = 1;
    public static final int EDIT_BUDGET = 2;
    public static final int DELETE_BUDGET = 3;
    public static final int GET_BUDGET_BY_MONTH = 4;

}
