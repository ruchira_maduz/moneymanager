package com.dfn.bm.connector.database.mapper.transactionmapper;

import com.dfn.bm.beans.Transaction;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionMapper  implements RowMapper<Transaction> {

    @Override
    public Transaction map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        Transaction transaction = new Transaction();
        transaction.setId(resultSet.getInt("ID"));
        transaction.setTransactionType(resultSet.getInt("TRANSACTION_TYPE"));
        transaction.setTransactionCategory(resultSet.getInt("TRANSACTION_CATEGORY"));
        transaction.setAmount(resultSet.getDouble("AMOUNT"));
        transaction.setDate(resultSet.getString("DATE"));
        transaction.setMemo(resultSet.getString("MEMO"));

        return transaction;
    }
}

