package com.dfn.bm.connector.database.dao;

import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 * Data Access Object for Creating Tables of DB.
 */

public interface TransactionManagerDAO {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS TRANSACTION_TABLE(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "TRANSACTION_TYPE INT(11)  NOT NULL, \n"
            + "TRANSACTION_CATEGORY INT(11)  NOT NULL, \n"
            + "AMOUNT DOUBLE  NOT NULL, \n"
            + "DATE DATE  NOT NULL, \n"
            + "MEMO TEXT DEFAULT NULL, \n"
            + "PRIMARY KEY (ID))")
    void createTransactionTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS CATEGORY_TABLE(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "TRANSACTION_CATEGORY INT(11)  NOT NULL, \n"
            + "NAME VARCHAR(100) DEFAULT NULL, \n"
            + "PRIMARY KEY (ID))")
    void createCategoryTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS BUDGET_TABLE(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "DATE DATE  NOT NULL, \n"
            + "AMOUNT DOUBLE  NOT NULL, \n"
            + "PRIMARY KEY (ID))")
    void createBudgetTable();

}
