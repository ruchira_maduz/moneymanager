package com.dfn.bm.connector.database;

import com.dfn.bm.util.settings.DBSettings;
import com.dfn.bm.util.settings.Settings;
import com.mysql.cj.jdbc.MysqlDataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;
import org.jdbi.v3.core.Jdbi;

import java.sql.SQLException;

/**
 * Store for managing DBI instance according to the current configuration.
 */
public final class DBUtilStore {

    private static final Logger LOGGER = LogManager.getLogger(DBUtilStore.class);
    private static final DBUtilStore INSTANCE = new DBUtilStore();

    public static DBUtilStore getInstance() {
        return INSTANCE;
    }
    private static HikariDataSource ds;

    private static Jdbi dbiInstance;

    private DBUtilStore() {
        DBSettings dbSettings = Settings.getInstance().getDbSettings();
        String connectionURL = "";
        JdbcConnectionPool connectionPool = null;
        switch (dbSettings.getDatabaseType().toLowerCase()) {
            case "mysql":
                try {
                    MysqlDataSource mysqlDataSource = new MysqlDataSource();
                    mysqlDataSource.setServerName(dbSettings.getDatabaseURL());
                    mysqlDataSource.setPortNumber(3306);
                    mysqlDataSource.setDatabaseName(dbSettings.getDatabaseName());
                    mysqlDataSource.setUser(dbSettings.getUsername());
                    mysqlDataSource.setPassword(dbSettings.getPassword());
                    mysqlDataSource.setLoginTimeout(dbSettings.getConnectionTimeOut());
                    mysqlDataSource.setRequireSSL(false);
                    mysqlDataSource.setUseSSL(false);
                    mysqlDataSource.setAllowPublicKeyRetrieval(true);

                    HikariConfig config = new HikariConfig();
                    config.setDataSource(mysqlDataSource);
                    config.setConnectionTimeout(600000); // 10min
                    config.setValidationTimeout(600000); // 10min
                    config.setAutoCommit(false);

                    ds = new HikariDataSource(config);
                    dbiInstance = Jdbi.create(ds);
                } catch (Exception e) {
                    LOGGER.error(e);
                    throw new RuntimeException(e);
                }
                break;
            case "oracle":
                connectionURL = "jdbc:oracle:" + dbSettings.getDriverType() + ":@"
                        + dbSettings.getDatabaseURL() + ":" + dbSettings.getDatabaseName();
                try {
                    OracleDataSource oracleSource = new OracleDataSource();
                    oracleSource.setURL(connectionURL);
                    oracleSource.setUser(dbSettings.getUsername());
                    oracleSource.setPassword(dbSettings.getPassword());
                    oracleSource.setLoginTimeout(dbSettings.getConnectionTimeOut());
                    oracleSource.setExplicitCachingEnabled(true);
                    oracleSource.setConnectionCachingEnabled(true);
                    oracleSource.setConnectionCacheName("CACHE");
                    dbiInstance = Jdbi.create(oracleSource);
                } catch (SQLException e) {
                    LOGGER.error(e);
                    throw new RuntimeException(e);
                }
                break;
            case "h2":
                connectionPool = JdbcConnectionPool.create("jdbc:h2:mem:" + dbSettings.getDatabaseName(),
                        dbSettings.getUsername(), dbSettings.getPassword());
                connectionPool.setLoginTimeout(dbSettings.getConnectionTimeOut());
                connectionPool.setMaxConnections(dbSettings.getBatchSize());

                dbiInstance = Jdbi.create(connectionPool);
                break;
            default:
                LOGGER.debug("Can't create DB connection");
        }
    }

    public Jdbi getDbiInstance() {
        return dbiInstance;
    }
}
