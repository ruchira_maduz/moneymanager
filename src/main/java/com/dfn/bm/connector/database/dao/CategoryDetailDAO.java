package com.dfn.bm.connector.database.dao;

import com.dfn.bm.beans.Category;
import com.dfn.bm.connector.database.mapper.categorymapper.CategoryMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;

public interface CategoryDetailDAO {

    @SqlQuery("SELECT * FROM CATEGORY_TABLE WHERE TRANSACTION_CATEGORY = :TRANSACTION_CATEGORY ")
    @UseRowMapper(CategoryMapper.class)
    List<Category> getCategoriesByTransactionCategory(@Bind("TRANSACTION_CATEGORY") int category);

    @SqlQuery("SELECT * FROM CATEGORY_TABLE")
    @UseRowMapper(CategoryMapper.class)
    List<Category> getAllCategories();


    @SqlUpdate("INSERT INTO CATEGORY_TABLE (TRANSACTION_CATEGORY, NAME)\n"
            + "VALUES (:TRANSACTION_CATEGORY , :NAME);")
    void addToCategoryTable(@Bind("TRANSACTION_CATEGORY") int transactionCategory,
                            @Bind("NAME") String name);


    @SqlUpdate("UPDATE CATEGORY_TABLE SET NAME = :NAME WHERE ID = :ID;")
    void updateCategoryTable(@Bind("ID") int id,
                             @Bind("NAME") String name);

    @SqlUpdate("DELETE FROM CATEGORY_TABLE WHERE ID = :ID;")
    void deleteCategoryByID(@Bind("ID") int id);
}
