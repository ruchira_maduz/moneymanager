package com.dfn.bm.connector.database;

import com.dfn.bm.connector.database.dao.TransactionManagerDAO;
import com.dfn.bm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;

/**
 * DB Content Handler.
 */

public final class DBContentHandler {
    private static final DBContentHandler INSTANCE = new DBContentHandler();

    private static final Logger LOGGER = LogManager.getLogger(DBContentHandler.class);
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();

    private DBContentHandler() {

    }

    public void initializeDBContent() {
        try (Handle handle = dbi.open()) {
            try {
                handle.begin();
                String dbType = Settings.getInstance().getDbSettings().getDatabaseType();
                if ("oracle".equals(dbType)) {
                    TransactionManagerDAO dao = handle.attach(TransactionManagerDAO.class);
                    DBContentHandler.getInstance().createTables(dao);
                } else if ("mysql".equals(dbType)) {
                    TransactionManagerDAO dao = handle.attach(TransactionManagerDAO.class);
                    DBContentHandler.getInstance().createTables(dao);
                } else if ("h2".equals(dbType)) {
                    TransactionManagerDAO dao = handle.attach(TransactionManagerDAO.class);
                    DBContentHandler.getInstance().createTables(dao);
                }
                handle.commit();
            } catch (Exception e) {
                LOGGER.error(e);
                handle.rollback();
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    public static DBContentHandler getInstance() {
        return INSTANCE;
    }

    public void createTables(TransactionManagerDAO dao) {
        dao.createTransactionTable();
        dao.createBudgetTable();
        dao.createCategoryTable();
    }
}
