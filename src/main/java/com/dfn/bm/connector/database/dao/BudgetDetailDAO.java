package com.dfn.bm.connector.database.dao;

import com.dfn.bm.beans.Budget;
import com.dfn.bm.connector.database.mapper.budgetmapper.BudgetMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;

public interface BudgetDetailDAO {

    @SqlQuery("SELECT * FROM BUDGET_TABLE WHERE MONTH(DATE)= :MONTH;")
    @UseRowMapper(BudgetMapper.class)
    Budget getBudgetByMonth(@Bind("MONTH") int month);

    @SqlQuery("SELECT * FROM BUDGET_TABLE")
    @UseRowMapper(BudgetMapper.class)
    List<Budget> getAllBudget();


    @SqlUpdate("INSERT INTO BUDGET_TABLE (DATE, AMOUNT)\n"
            + "VALUES (:DATE , :AMOUNT);")
    void addBudgetTable(@Bind("DATE") String date,
                            @Bind("AMOUNT") double amount);


    @SqlUpdate("UPDATE BUDGET_TABLE SET AMOUNT = :AMOUNT WHERE ID = :ID;")
    void updateBudgetTable(@Bind("ID") int id,
                             @Bind("AMOUNT") double amount);

    @SqlUpdate("DELETE FROM BUDGET_TABLE WHERE ID = :ID;")
    void deleteBudgetByID(@Bind("ID") int id);

}
