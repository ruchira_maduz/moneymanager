package com.dfn.bm.connector.database.mapper.budgetmapper;

import com.dfn.bm.beans.Budget;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BudgetMapper implements RowMapper<Budget> {

    @Override
    public Budget map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        Budget budget = new Budget();
        budget.setId(resultSet.getInt("ID"));
        budget.setAmount(resultSet.getDouble("AMOUNT"));
        budget.setDate(resultSet.getString("DATE"));

        return budget;
    }
}


