package com.dfn.bm.connector.database.dao;

import com.dfn.bm.beans.Transaction;
import com.dfn.bm.connector.database.mapper.transactionmapper.TransactionMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;

public interface TransactionDetailDAO {

    @SqlUpdate("INSERT INTO TRANSACTION_TABLE (TRANSACTION_TYPE, TRANSACTION_CATEGORY, AMOUNT,DATE, MEMO)\n"
            + "VALUES (:TRANSACTION_TYPE, :TRANSACTION_CATEGORY, :AMOUNT, :DATE, :MEMO);")
    void addToTransactionTable(@Bind("TRANSACTION_TYPE") int transactionType,
                               @Bind("TRANSACTION_CATEGORY") int transactionCategory,
                               @Bind("AMOUNT") double amount,
                               @Bind("DATE") String date,
                               @Bind("MEMO") String memo);

    @SqlQuery("SELECT * FROM TRANSACTION_TABLE WHERE MONTH(DATE)= :MONTH; ")
    @UseRowMapper(TransactionMapper.class)
    List<Transaction> getTransactionsByMonth(@Bind("MONTH") int month);

    @SqlQuery("SELECT * FROM TRANSACTION_TABLE ")
    @UseRowMapper(TransactionMapper.class)
    List<Transaction> getAllTransactions();

    @SqlQuery("SELECT * FROM TRANSACTION_TABLE WHERE TRANSACTION_TYPE = :TRANSACTION_TYPE ")
    @UseRowMapper(TransactionMapper.class)
    List<Transaction> getTransactionsByTransactionType(@Bind("TRANSACTION_TYPE") int transactionType);

    @SqlQuery("SELECT * FROM TRANSACTION_TABLE WHERE TRANSACTION_TYPE = :TRANSACTION_TYPE AND MONTH(DATE)= :MONTH; ")
    @UseRowMapper(TransactionMapper.class)
    List<Transaction> getTransactionsByTransactionTypeAndMonth(@Bind("TRANSACTION_TYPE") int transactionType, @Bind("MONTH") int month);


    @SqlUpdate("UPDATE TRANSACTION_TABLE SET AMOUNT = :AMOUNT , TRANSACTION_TYPE = :TRANSACTION_TYPE, "
           + "TRANSACTION_CATEGORY = :TRANSACTION_CATEGORY, DATE = :DATE, MEMO = :MEMO WHERE ID = :ID;")
    void updateTransactionTable(@Bind("ID") int id,
                                @Bind("TRANSACTION_TYPE") int transactionType,
                                @Bind("TRANSACTION_CATEGORY") int transactionCategory,
                                @Bind("AMOUNT") double amount,
                                @Bind("DATE") String date,
                                @Bind("MEMO") String memo);

    @SqlUpdate("DELETE FROM TRANSACTION_TABLE WHERE ID = :ID;")
    void deleteTransactionByID(@Bind("ID") int id);


}
