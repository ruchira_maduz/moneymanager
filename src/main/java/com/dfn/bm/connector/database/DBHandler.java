package com.dfn.bm.connector.database;


import com.dfn.bm.beans.Budget;
import com.dfn.bm.beans.Category;
import com.dfn.bm.beans.Transaction;
import com.dfn.bm.connector.database.dao.BudgetDetailDAO;
import com.dfn.bm.connector.database.dao.CategoryDetailDAO;
import com.dfn.bm.connector.database.dao.TransactionDetailDAO;
import com.dfn.bm.util.exception.TransactionManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

/**
 * DB Handler.
 */

public final class DBHandler {

    private static final Logger LOGGER = LogManager.getLogger(DBHandler.class);

    private static final DBHandler INSTANCE = new DBHandler();
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();

    private DBHandler() {

    }

    public static DBHandler getInstance() {
        return INSTANCE;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void insertIntoTransactionTable(Handle handle, Transaction transaction) {
        try {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            transactionDetailDAO.addToTransactionTable(transaction.getTransactionType(),
                    transaction.getTransactionCategory(), transaction.getAmount(), transaction.getDate(),
                    transaction.getMemo());

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public void updateTransactionTable(Handle handle, Transaction transaction, int id) {
        try {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            transactionDetailDAO.updateTransactionTable(id, transaction.getTransactionType(),
                    transaction.getTransactionCategory(), transaction.getAmount(), transaction.getDate(),
                    transaction.getMemo());

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public void deleteFromTransactionTable(Handle handle, int id) {
        try {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            transactionDetailDAO.deleteTransactionByID(id);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public List<Transaction> getTransactionByMonth(int mont) {
        try (Handle handle = dbi.open()) {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            return transactionDetailDAO.getTransactionsByMonth(mont);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public List<Transaction> getAllTransaction() {
        try (Handle handle = dbi.open()) {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            return transactionDetailDAO.getAllTransactions();

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public List<Transaction> getTransactionsByTransactionType(int transactionType) {
        try (Handle handle = dbi.open()) {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            return transactionDetailDAO.getTransactionsByTransactionType(transactionType);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public List<Transaction> getTransactionsByTransactionTypeAndMonth(int transactionType, int month) {
        try (Handle handle = dbi.open()) {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            return transactionDetailDAO.getTransactionsByTransactionTypeAndMonth(transactionType, month);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void insertIntoCategoryTable(Handle handle, Category category) {
        try {
            CategoryDetailDAO categoryDetailDAO = handle.attach(CategoryDetailDAO.class);
            categoryDetailDAO.addToCategoryTable(category.getTransactionCategory(), category.getName());

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public void updateCategoryTable(Handle handle, String name, int id) {
        try {
            CategoryDetailDAO categoryDetailDAO = handle.attach(CategoryDetailDAO.class);
            categoryDetailDAO.updateCategoryTable(id, name);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public void deleteFromCategoryTable(Handle handle, int id) {
        try {
            CategoryDetailDAO categoryDetailDAO = handle.attach(CategoryDetailDAO.class);
            categoryDetailDAO.deleteCategoryByID(id);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public List<Category> getCategoryByTransactionCategory(int category) {
        try (Handle handle = dbi.open()) {
            CategoryDetailDAO categoryDetailDAO = handle.attach(CategoryDetailDAO.class);
            return categoryDetailDAO.getCategoriesByTransactionCategory(category);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public List<Category> getAllCategory() {
        try (Handle handle = dbi.open()) {
            CategoryDetailDAO categoryDetailDAO = handle.attach(CategoryDetailDAO.class);
            return categoryDetailDAO.getAllCategories();

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void insertIntoBudgetTable(Handle handle, Budget budget) {
        try {
            BudgetDetailDAO budgetDetailDAO = handle.attach(BudgetDetailDAO.class);
            budgetDetailDAO.addBudgetTable(budget.getDate(), budget.getAmount());

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public void updateBudgetTable(Handle handle, double amount, int id) {
        try {
            BudgetDetailDAO budgetDetailDAO = handle.attach(BudgetDetailDAO.class);
            budgetDetailDAO.updateBudgetTable(id, amount);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public void deleteFromBudgetTable(Handle handle, int id) {
        try {
            BudgetDetailDAO budgetDetailDAO = handle.attach(BudgetDetailDAO.class);
            budgetDetailDAO.deleteBudgetByID(id);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public Budget getBudgetByMonth(int month) {
        try (Handle handle = dbi.open()) {
            BudgetDetailDAO budgetDetailDAO = handle.attach(BudgetDetailDAO.class);
            return budgetDetailDAO.getBudgetByMonth(month);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }

    public List<Budget> getAllBudget() {
        try (Handle handle = dbi.open()) {
            BudgetDetailDAO budgetDetailDAO = handle.attach(BudgetDetailDAO.class);
            return budgetDetailDAO.getAllBudget();

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new TransactionManagerRuntimeException();
        }
    }
}

