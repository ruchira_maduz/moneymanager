package com.dfn.bm.connector.database.mapper.categorymapper;

import com.dfn.bm.beans.Category;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryMapper implements RowMapper<Category> {

    @Override
    public Category map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        Category category = new Category();
        category.setId(resultSet.getInt("ID"));
        category.setName(resultSet.getString("NAME"));
        category.setTransactionCategory(resultSet.getInt("TRANSACTION_CATEGORY"));

        return category;
    }
}