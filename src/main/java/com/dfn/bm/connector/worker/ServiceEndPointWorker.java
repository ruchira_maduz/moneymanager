package com.dfn.bm.connector.worker;

import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.PreRestart;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.dfn.bm.actors.messages.ServicesBaseRequest;
import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.exception.TransactionManagerRuntimeException;
import com.dfn.bm.util.message.Command;
import com.dfn.bm.util.service.ServicesProcessorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

public class ServiceEndPointWorker extends AbstractBehavior<Command> {

    private static final Logger LOGGER = LogManager.getLogger(ServiceEndPointWorker.class);

    public ServiceEndPointWorker(ActorContext<Command> context) {
        super(context);
    }

    public static Behavior<Command> create(String functionID) {
        LOGGER.info("HTTP Endpoint Worker is started. FunctionID: {}", functionID);
        return Behaviors.setup(ServiceEndPointWorker::new);
    }


    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder().
                onMessage(ServicesBaseRequest.class, this::onHttpRequest).
                onSignal(PreRestart.class, signal -> onPreStart()).
                onSignal(PostStop.class, signal -> onPostStop()).build();
    }


    protected Behavior<Command> onHttpRequest(ServicesBaseRequest servicesCommand) {
        try {
            JSONObject params = servicesCommand.getRequest();
            int functionId = servicesCommand.getFunctionId();
            String serviceType = servicesCommand.getServiceType();

            if (serviceType == null) {
                throw new TransactionManagerRuntimeException("Function ID Undefined");
            }

            Object result = ServicesProcessorFactory.getProcessor(serviceType).process(params, functionId);

            if (result instanceof Command) {
                Command reply = (Command) result;
                servicesCommand.getReplyTo().tell(reply);
            } else {
                LOGGER.info("Service processor completed: But unknown response type. {} | {}",
                        functionId, params);
            }

        } catch (Exception e) {
            try {
                ServicesBaseResponse response;
                JSONObject responseData = new JSONObject();
                responseData.put("Error", e.getMessage());
                response = ServicesUtills.getResponse(false, responseData);
                servicesCommand.getReplyTo().tell(response);
            } catch (Exception ex) {
                LOGGER.error("Error in Services Endpoint, {}", e, e);
            }
        }
        return Behaviors.same();
    }

    public ServiceEndPointWorker onPostStop() {
        LOGGER.info("Services HTTP EndpointWorker is stopped.");
        return this;
    }

    public ServiceEndPointWorker onPreStart() {
        LOGGER.info("Services HTTP Endpoint Worker is PreStart.");
        return this;
    }
}