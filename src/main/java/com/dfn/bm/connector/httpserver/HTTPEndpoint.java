package com.dfn.bm.connector.httpserver;


import akka.actor.typed.ActorSystem;
import akka.http.javadsl.ConnectionContext;
import akka.http.javadsl.Http;
import akka.http.javadsl.HttpsConnectionContext;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.marshallers.jackson.Jackson;

import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;

import ch.megard.akka.http.cors.javadsl.CorsDirectives;

import com.dfn.bm.actors.messages.HTTPRequestCommand;
import com.dfn.bm.util.ActorUtils;
import com.dfn.bm.util.Constants;
import com.dfn.bm.util.HTTPServerPostStartAction;
import com.dfn.bm.util.SecureConnectionSettings;
import com.dfn.bm.util.message.Command;
import com.dfn.bm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;

import static akka.actor.typed.javadsl.AskPattern.ask;


public class HTTPEndpoint extends AllDirectives {

    private static final HTTPEndpoint INSTANCE = new HTTPEndpoint();
    private static final Logger LOGGER = LogManager.getLogger(HTTPEndpoint.class);

    private final String server = Settings.getInstance().getHttpEndpointSettings().getServer();
    private final int port = Settings.getInstance().getHttpEndpointSettings().getPort();
    private akka.actor.typed.ActorSystem<Command> system;
    private CompletionStage<ServerBinding> binding;
    private List<Route> httpRoutes = new ArrayList<>();
    private boolean isInit = false;
    private List<HTTPServerPostStartAction> postStartActions = new ArrayList<>();

    public HTTPEndpoint() {
        this.system = ActorUtils.getRootSupervisor();
    }

    public static HTTPEndpoint getSharedInstance() {
        return INSTANCE;
    }


    public void initialize() {

        try {
            final Http http = Http.get(system);
            if (!Settings.getInstance().getHttpEndpointSettings().getSecureConnection()) {
                binding = http.newServerAt(server, port)
                        .bind(this.createRoute());
            } else {
                binding = http.newServerAt(server, port)
                        .enableHttps(createHttpsContext(system))
                        .bind(this.createRoute());
            }
            LOGGER.debug("HTTP server is start in {} port {}", server, port);
        } catch (Exception e) {
            LOGGER.error("HTTP server is unable to start in {} port {}", server, port, e);
        }

    }

    private HttpsConnectionContext createHttpsContext(ActorSystem<Command> system) {
        try {
            SecureConnectionSettings secureConnectionSettings = Settings.getInstance().getHttpEndpointSettings()
                    .getSecureConnectionSettings();

            String passwordStr = "";
            if (secureConnectionSettings.getPassPhraseEncrypt()) {
                // decrypt the password
                passwordStr = null;
            } else {
                // use the plain password.
                passwordStr = secureConnectionSettings.getPassPhrase();
            }
            final char[] password = passwordStr.toCharArray();
            final KeyStore ks = KeyStore.getInstance("PKCS12");
            final InputStream keystore = null;
            if (keystore == null) {
                // throw new AMLRuntimeException("Keystore required!");
            }
            ks.load(keystore, password);

            final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            keyManagerFactory.init(ks, password);

            final TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);

            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

            return ConnectionContext.httpsServer(sslContext);

        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    private Route createRoute() {

        Route first = getHttpRoutes().get(0);
        Route[] rest = new Route[getHttpRoutes().size() - 1];
        for (int i = 1; i < getHttpRoutes().size(); i++) {
            rest[i - 1] = getHttpRoutes().get(i);
        }

        return concat(first, rest);
    }


    public void addServicesAPIRequestRoute() {
        this.httpRoutes.add(
                CorsDirectives.cors(() ->
                        post(() -> path("ServicesAPI", () ->
                                        entity(Jackson.unmarshaller(JSONObject.class), req ->
                                                headerValueByName(Constants.HEADER_HOST, headVal -> {
                                                    String serviceType = req.get(Constants.SERVICE_TYPE).toString();
                                                    int functionId = ((Number) req.get(Constants.FUNCTION_ID)).intValue();

                                                    CompletionStage<Command> response = ask(ActorUtils.getServicesSupervisor(),
                                                            replyTo -> new HTTPRequestCommand(serviceType, functionId
                                                                    , replyTo, req), Duration.ofSeconds(30), system.scheduler());
                                                    return completeOKWithFuture(response, Jackson.marshaller());

                                                }))
                        )))

        );
    }

    public void unbindThePort() {
        if (binding != null) {
            binding.thenCompose(ServerBinding::unbind); // trigger unbinding from the port
        }
    }

    public List<Route> getHttpRoutes() {
        return httpRoutes;
    }

}

